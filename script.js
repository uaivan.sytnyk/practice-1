//Завдання 1
// Дано: Функція яка приймає значення яке може бути будь-яким типом даних = "302", null, 128, 60
// Результат: Вивести у консоль числа які є степенем двійки та не більші заданого значення або вивести помилку про коректний тип якщо значення не є числом
// Приклад:
// 60 - 1, 2, 4, 8, 16, 32
// 64 - 1, 2, 4, 8, 16, 32, 64
// “32” - "incorrect type"

let printPowsOf2 = (num) => {
  if (typeof num != "number") {
    console.log("Type is not a number");
  } else {
    for (let i = 1; i < num; i = i * 2) {
      console.log(i);
    }
  }
}

printPowsOf2(16);

//Завдання 2
// Дано: масив [3, 2, "2", null, 1.5, 9.5, undefined];
// Результат: Вивести у консоль суму чисел у масиві.

let calculateSumOfArray = () => {
  const initialArray = [3, 2, "2", null, 1.5, 9.5, undefined];
  let sum = 0;
  for (num of initialArray) {
    if(typeof num == "number")
    sum = sum + num;
  }
  console.log(sum);
 }

calculateSumOfArray();

//Завдання 3
// Дано: Функція яка приймає назва місяця у форматі = "DECEMBER" | "FEBRUARY" | "JULY" і тд.
// Результат: Вивести у консоль назву пори року якій відповідає даний місяць. “summer” | “autumn” | “winter” | “spring”.

let printSeasonByMonth = (month) => {
  switch (month) {
    case "JUNE":
    case "JULY":
    case "August":
      console.log("summer");
      break;
    case "SEPTEMBER":
    case "OCTOBER":
    case "NOVEMBER":
      console.log("autumn");
      break;
    case "DECEMBER":
    case "JANUARY":
    case "FEBRUARY":
      console.log("winter");
      break;
    case "MARCH":
    case "APRIL":
    case "MAY":
      console.log("spring");
      break;
    default:
      console.log("Write month in uppercase or it is not a month");
  }
 }

 printSeasonByMonth("SEPTEMBER");
 printSeasonByMonth("NOVEMBER");
 printSeasonByMonth("JULY");
 printSeasonByMonth("APRIL");

//Завдання 4
// Дано: Функція яка приймає стрічку яка містить мінімум 1 слово та пробіли. Перший та останній символ не пробіли. Слова можуть бути розділені не лише одним пробілом.
// Результат: Вивести у консоль кількість слів у стрічці.
// Приклад:
// "Easy string for count" - 4 слова
// "Some string with a triple space" - 6 слів

let calculateWordsInString = (string) => {
  let splitStr = string.split(" ");
  let i = 0;
  for ( str of splitStr) {

    if (str) {
      i++
    }
  }
  console.log(i);
}

calculateWordsInString("Easy string for count");
calculateWordsInString("Easy");
calculateWordsInString("Some string with a triple   space");
calculateWordsInString("Some?  string, with a triple   space");